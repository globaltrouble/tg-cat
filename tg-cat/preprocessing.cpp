#include <vector>
#include <iostream>
#include <unordered_set>
#include <locale>
#include <codecvt>
#include <algorithm>
#include <numeric>
#include <exception>

#include <re2/re2.h>

#include "preprocessing.h"
#include "symbols_to_replace.h"


#define DEFAULT_REPLACEMENT " "


using UnicodeConverter = std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>;

static const char* REGEXP_PATTERN (
    "(https?://)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)"  // url
    "|([a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*)"  // email
    "|((#|@)[a-zA-Z0-9_]+)"     // hashtag or nickname
//        "([\t\n\r])"    // blanks, to slow regex
//        "|(-?\\d+)"    // digits, alredy included
    "|((\\S)+\\.((txt)|(docx?)|(jpe?g)|(png)|(pdf)))"    // common files
);

static char32_t const ASCII_TO_REPLACE[] =
{
    '\n',
    '\r',
    '\t',
    ' '
};

static bool was_init = false;

static std::unique_ptr<std::unordered_set<char32_t>> unicode_to_replace;
static std::unique_ptr<RE2> re;
static std::unique_ptr<UnicodeConverter> u_converter;
static const char32_t replacement = DEFAULT_REPLACEMENT[0];


void init_preprocessing()
{
    if (was_init) return;

    re = std::unique_ptr<RE2>(new RE2(REGEXP_PATTERN));
    if (!re->ok())
        throw std::runtime_error(std::string("Invalid regex: ") + re->pattern());

    unicode_to_replace = std::unique_ptr<std::unordered_set<char32_t>>(new std::unordered_set<char32_t>());
    unicode_to_replace->reserve((sizeof(ASCII_TO_REPLACE) + sizeof(SYMBOLS_TO_REPLACE)) / sizeof (char32_t));

    for (auto s : ASCII_TO_REPLACE)
        unicode_to_replace->insert(s);

    for (auto s : SYMBOLS_TO_REPLACE)
        unicode_to_replace->insert(s);

    u_converter = std::unique_ptr<UnicodeConverter>(new UnicodeConverter());

    was_init = true;
}


void normalize(std::string& source) noexcept
{
    if (source.empty()) return;

    RE2::GlobalReplace(&source, *re, DEFAULT_REPLACEMENT);

    std::u32string unicode = u_converter->from_bytes(source.c_str(), source.c_str() + source.length());

    std::vector<char32_t> replaced;
    replaced.reserve(unicode.length() + 1);

    size_t last = 0;
    size_t i = 0;
    int dupplicates = 0;
    for (; i < unicode.length(); ++i)
    {
        if (i > 0 && unicode[i - 1] == unicode[i])
            ++dupplicates;
        else
            dupplicates = 0;

        // check replacement using hashtable, instead of regex, to speedup search for 4000+ symbols.
        if (unicode_to_replace->find(unicode[i]) != unicode_to_replace->end() || dupplicates > 1)
        {
            if (last != i)
            {
                std::transform(unicode.begin() + last,
                               unicode.begin() + i,
                               std::back_inserter(replaced),
                               char32_to_lower);
                replaced.push_back(replacement);
            }

            last = i + 1;
        }
    }
    std::transform(unicode.begin() + last,
                   unicode.begin() + i,
                   std::back_inserter(replaced),
                   char32_to_lower);
    replaced.push_back('\0');

    std::string result = u_converter->to_bytes(replaced.data(), replaced.data() + replaced.size());
    result.swap(source);
}


std::string join_texts(const char** strings, int count, const char* sep) noexcept
{
    std::vector<int> sizes;
    sizes.reserve(count);

    std::transform(strings, strings + count, std::back_inserter(sizes), strlen);
    // count is separator for each string and trailing '\0'
    int total_size = std::accumulate(sizes.begin(), sizes.end(), count);

    std::string result;
    result.reserve(total_size);

    for (int i = 0; i < count; ++i)
    {
        result.append(strings[i], sizes[i]);
        result.append(sep, strlen(sep));
    }

    return result;
}


char32_t char32_to_lower(char32_t c) noexcept
{
    // we can just use ICU or BOOST, but due to requirements lib must avoid external libraries and work as fast as possible
    if ((c >= 65 && c <= 90) || (c >= 1040 && c <= 1071))
    {
        return c + 32;
    }
    else if (c == 1105)
    {
        // Ё 1025 -> ё 1105
        return c + 80;
    }
    return c;
}
