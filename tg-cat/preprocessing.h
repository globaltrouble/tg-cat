#ifndef PREPROCESSING_H
#define PREPROCESSING_H

#include <memory>


/**
 * Initiates preprocessing, must be called before every other function call from this header.
 */
void init_preprocessing();


/**
 * Removes redundant symbols from string, for example: smiles, emoji, flags, some punctuation.
 * \param[in/out] source: string that will be normalized in place.
 */
void normalize(std::string& source) noexcept;


/**
 * Joins an array of cstrings to a one std::string.
 * \param[in] strings: an array to be joined.
 * \param[in] count: array size.
 * \param[in] separator: string that will be inserted between joined string.
 * \return std::string final joined string.
 */
std::string join_texts(const char** strings, int count, const char* sep = " ") noexcept;


/**
 * Transforms char to lowercase. This also can be done by ICU library, but:
 * 1) it's to slow to convert from char32 to ICU::unicode, then transform result and do and then vise wersa convertation.
 * 2) accordingly to competition rules, library must use as few additional lib dependencies, as possible.
 * \param[in] c: char for transformation.
 * \return transformed char.
 */
char32_t char32_to_lower(char32_t c) noexcept;

#endif // PREPROCESSING_H
