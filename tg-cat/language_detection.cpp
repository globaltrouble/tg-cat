#include <sstream>
#include <iostream>
#include <cstring>

#include <dirent.h>
#include <stdlib.h>
#include <unistd.h>

#include "fasttext/fasttext.h"

#define PREFIX "__label__"
#define TOP_K_LANGUAGES 3
#define THRETHOLD 0.0


static std::unique_ptr<fasttext::FastText> model;
static bool was_init = false;


void init_language_detection_model()
{
    if (was_init) return;

    model = std::unique_ptr<fasttext::FastText>(new fasttext::FastText());
    model->loadModel("resources/lid.176.bin");

    was_init = true;
}


void detect_language(const std::string& s, std::vector<std::pair<float, std::string>>& results, float p_threthold)
{
    results.clear();
    results.reserve(TOP_K_LANGUAGES);

    std::vector<std::pair<fasttext::real, std::string>> preds;
    std::istringstream ss(s);

    model->predictLine(ss, preds, TOP_K_LANGUAGES, THRETHOLD);

    for (auto& pred : preds)
    {
        if (pred.first < p_threthold) continue;

        results.emplace_back(
            std::make_pair(
                pred.first,
                pred.second.substr(strlen(PREFIX), 2)
            )
        );
    }
}
