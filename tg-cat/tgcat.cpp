#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <memory>

#include "tgcat.h"
#include "preprocessing.h"
#include "language_detection.h"
#include "topic_modeling.h"


#define DEFAULT_LANG "other"


int tgcat_init()
{
    try
    {
        init_preprocessing();
        init_language_detection_model();
        init_topic_modeling();
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    return 0;
}


int tgcat_detect_language(const struct TelegramChannelInfo *channel_info,
                          char language_code[6])
{
    // todo: error handling
    const char** texts = static_cast<const char**>(malloc((channel_info->post_count + 2) * sizeof(char*)));
    texts[0] = channel_info->title;
    texts[1] = channel_info->description;

    memcpy(texts + 2, channel_info->posts, channel_info->post_count * sizeof (char *));
    auto text = join_texts(texts, channel_info->post_count + 2);
    normalize(text);

    std::vector<std::pair<float, std::string>> results;
    detect_language(text, results);

    const char* lang = results.empty() ? DEFAULT_LANG : results.front().second.c_str();
    strcpy(language_code, lang);

    return 0;
}

int tgcat_detect_category(const struct TelegramChannelInfo *channel_info,
                          double category_probability[TGCAT_CATEGORY_OTHER + 1])
{
    size_t size = TGCAT_CATEGORY_OTHER + 1;
    memset(category_probability, 0, sizeof(double) * size);
    const float p_threshold = 0.25;
    size_t pred_count = 0;

    std::string title(channel_info->title);
    normalize(title);

    std::vector<std::pair<float, std::string>> langs;
    detect_language(title, langs, p_threshold);

    if (!langs.empty())
        pred_count += LDAModel::predict(title, langs.front().second, category_probability);

    std::string desc(channel_info->description);
    normalize(desc);

    langs.clear();
    detect_language(desc, langs, p_threshold);

    if (!langs.empty())
        pred_count += LDAModel::predict(desc, langs.front().second, category_probability);

    for (size_t i = 0; i < channel_info->post_count; ++i)
    {
        std::string text(channel_info->posts[i]);
        normalize(text);

        langs.clear();
        detect_language(text, langs, p_threshold);
        if (langs.empty()) continue;

        pred_count += LDAModel::predict(text, langs.front().second, category_probability);
    }

    if (pred_count == 0) return 1;

    double total = std::accumulate(category_probability, category_probability + size, 0);
    for (size_t i = 0; i <= TGCAT_CATEGORY_OTHER; ++i)
    {
        category_probability[i] /= total;
    }

    return 0;
}
