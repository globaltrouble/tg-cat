#include <fstream>
#include <iostream>

#include <sstream>
#include <iterator>
#include <algorithm>
#include <functional>
#include <stdexcept>

#include "topic_modeling.h"

#define STEMMER_ENCODING "UTF_8"
#define MODEL_TYPE_RU "ru"
#define MODEL_TYPE_EN "en"


bool was_init = false;
static std::unique_ptr<LDAModel> lda_ru;
static std::unique_ptr<LDAModel> lda_en;


void init_topic_modeling()
{
    if (was_init) return;

    load_model_ru();
    load_model_en();

    was_init = true;
}

void load_model_ru()
{
    std::string model_path("resources/lda_model_ru.txt");
    std::string dict_path("resources/dict_ru.txt");
    std::string stop_words_path("resources/stopwords_ru.txt");
    std::string mapping("resources/maping_en.txt");

    lda_ru = std::unique_ptr<LDAModel>(new LDAModel(MODEL_TYPE_RU,
                                       model_path,
                                       dict_path,
                                       stop_words_path,
                                       mapping));
}


void load_model_en()
{
    std::string model_path("resources/lda_model_en.txt");
    std::string dict_path("resources/dict_en.txt");
    std::string stop_words_path("resources/stopwords_en.txt");
    std::string mapping("resources/maping_en.txt");

    lda_en = std::unique_ptr<LDAModel>(new LDAModel(MODEL_TYPE_EN,
                                       model_path,
                                       dict_path,
                                       stop_words_path,
                                       mapping));
}


LDAModel::LDAModel(const std::string& lang,
                   const std::string& model_path,
                   const std::string& dict_path,
                   const std::string& stop_words,
                   const std::string& mapping_path)
    : stemmer(sb_stemmer_new(lang.c_str(), STEMMER_ENCODING), sb_stemmer_delete)
    , gamma_dist(std::gamma_distribution<float>(100, 0.01))
{
    load_model(model_path);
    load_dict(dict_path);
    load_stop_words(stop_words);
    load_mapping(mapping_path);

    gen = std::unique_ptr<std::mt19937>(new std::mt19937(rd()));

    if (word_dict.size() != static_cast<size_t>(m.cols()))
        throw std::runtime_error(std::string("Dict!=model, dict=" + std::to_string(word_dict.size())) + ",cols=" + std::to_string(m.cols()));
};


void LDAModel::load_model(const std::string& model_path)
{
    std::fstream config(model_path);
    std::string buff;
    config >> buff;
    if (buff.empty())
        throw std::runtime_error(std::string("Err load model, can't load topics ") + model_path);

    topics = atoll(buff.c_str());
    if (!topics)
        throw std::runtime_error(std::string("Err load model, wrong topics: ")  + std::to_string(topics) + " " + model_path);

    buff.clear();
    config >> buff;
    if (buff.empty())
        throw std::runtime_error(std::string("Err load model, can't load words: ") + model_path);

    words = atoll(buff.c_str());
    if (!words)
        throw std::runtime_error(std::string("Err load model, wrong words: ")  + std::to_string(words) + " " + model_path);

    m = Eigen::MatrixXf(topics, words);
    for (size_t i = 0; i < topics * words; ++i)
    {
        buff.clear();
        config >> buff;
        if (buff.empty())
            throw std::runtime_error(std::string("Err load model, can't load matrix at i=")  + std::to_string(i) + " " + model_path);

        float val = atof(buff.c_str());
        if (!val)
            throw std::runtime_error(std::string("Err load model, can't load matrix, val=0 at i=")  + std::to_string(i) + " " + model_path);

        m(i / words, i % words) = val;

    }

    alpha = Eigen::VectorXf(topics);
    for (size_t i = 0; i < topics; ++i)
    {
        buff.clear();
        config >> buff;
        if (buff.empty())
            throw std::runtime_error(std::string("Err load model, can't load alpha at i=")  + std::to_string(i) + " " + model_path);

        float val = atof(buff.c_str());
        if (!val)
            throw std::runtime_error(std::string("Err load model, can't load alpha,  val=0 at i=")  + std::to_string(i) + " " + model_path);

        alpha(i) = val;
    }

    buff.clear();
    config >> buff;
    if (buff.empty())
        throw std::runtime_error(std::string("Err load model, can't load iteration_to_converge ") + model_path);

    iteration_to_converge = atoll(buff.c_str());
    if (!iteration_to_converge)
        throw std::runtime_error(std::string("Err load model,iteration_to_converge = 0") + model_path);

    buff.clear();
    config >> buff;
    if (buff.empty())
        throw std::runtime_error(std::string("Err load model, can't load gamma_threshold") + model_path);

    gamma_threshold = atof(buff.c_str());
    if (gamma_threshold <= 0)
        throw std::runtime_error(std::string("Err load model, can't load gamma_threshold: ") + std::to_string(gamma_threshold) + " " + model_path);
}



void LDAModel::load_dict(const std::string& dict_path)
{
    std::fstream config(dict_path);
    std::string buff;
    config >> buff;
    if (buff.empty())
        throw std::runtime_error(std::string("Err load dict, can't load size ") + dict_path);

    size_t size = atoll(buff.c_str());
    if (!size)
        throw std::runtime_error(std::string("Err load dict, can't load size: ") + std::to_string(size) + " " + dict_path);

    word_dict.reserve(size);
    for (size_t i = 0; i < size; ++i)
    {
        buff.clear();
        config >> buff;

        if (buff.empty())
            throw std::runtime_error(std::string("Err load dict, can't load word at i=") + std::to_string(i) + " " + dict_path);

        if (word_dict.find(buff) != word_dict.end())
            throw std::runtime_error(std::string("Err load dict, word already exists: ") + buff + " " + dict_path);

        word_dict.insert({buff, i});
    }
}


void LDAModel::load_stop_words(const std::string& stop_words_path)
{
    std::fstream config(stop_words_path);
    std::string buff;
    config >> buff;
    if (buff.empty())
        throw std::runtime_error(std::string("Err load stop_words, can't load size!") + stop_words_path);

    size_t size = atoll(buff.c_str());
    if (!size)
        throw std::runtime_error(std::string("Err load stop_words, can't load size: ") + std::to_string(size) + " " + stop_words_path);

    stop_words.reserve(size);
    for (size_t i = 0; i < size; ++i)
    {
        buff.clear();
        config >> buff;

        if (buff.empty())
            throw std::runtime_error(std::string("Err load stop_words, can't load word at i=") + std::to_string(i) + " " + stop_words_path);

        if (stop_words.find(buff) != stop_words.end())
            throw std::runtime_error(std::string("Err load stop_words, word already exists: ") + buff + " " + stop_words_path);

        stop_words.insert(buff);
    }
}

void LDAModel::load_mapping(const std::string& path)
{
    std::fstream config(path);
    std::string buff;
    config >> buff;
    if (buff.empty())
        throw std::runtime_error(std::string("Err load_mapping, can't load size! ") + path);

    size_t size = atoll(buff.c_str());
    if (!size)
        throw std::runtime_error(std::string("Err load_mapping, can't load size = 0;") + path);

    for (size_t i = 0; i < size; ++i)
    {
        buff.clear();
        config >> buff;

        if (buff.empty())
            throw std::runtime_error(std::string("Err load_mapping, can't load size! ") + std::to_string(i) + " " + path);

        size_t cluster = atoll(buff.c_str());

        mapping.insert({cluster, std::vector<size_t>()});

        buff.clear();
        config >> buff;
        if (buff.empty())
            throw std::runtime_error(std::string("Err load_mapping, can't load cluster size! ") + std::to_string(i) + " " + path);

        size_t count = atoll(buff.c_str());
        if (!count)
            throw std::runtime_error(std::string("Err load_mapping, can't load cluster size == 0!! ") + std::to_string(i) + " " + path);

        for (size_t j = 0; j < count; ++j)
        {
            buff.clear();
            config >> buff;

            size_t topic = atoll(buff.c_str());
            mapping[cluster].push_back(topic);
        }
    }
}



bool LDAModel::predict(const std::string& text, const std::string& lang, double* category_probability) noexcept
{
    LDAModel* model;

    // models are hardcoded, because we are not going to extend this functionality
    // but the whole set of supported languages can be read from a resource file
    // and then each model can be loaded to map<LANG_CODE, LDAModel>
    if (strcmp(lang.c_str(), MODEL_TYPE_RU) == 0)
        model = lda_ru.get();
    else if (strcmp(lang.c_str(), MODEL_TYPE_EN) == 0)
        model = lda_en.get();
    else
        return false;

    LDAModel::BoW bow;
    model->to_bow(text, bow);

    LDAModel::Predictions preds;
    model->topics_for_bow(bow, preds);

    if (preds.empty()) return false;

    for (auto& pred : preds)
    {
        for (size_t topic_id : model->mapping[pred.first])
        {
            category_probability[topic_id] += pred.second;
        }
    }

    return true;
}


void LDAModel::to_bow(const std::string& text, BoW& out) noexcept
{
    if (text.empty()) return;

    std::istringstream iss(text);
    std::vector<std::string> splitted((std::istream_iterator<std::string> {iss}),
                                      std::istream_iterator<std::string>());

    std::unordered_map<WordId, size_t> freq;
    freq.reserve(splitted.size());

    for (auto& word : splitted)
    {
        if (word.empty()) continue;

        if (stop_words.find(word) != stop_words.end()) continue;

        std::string stemmed = stem(word);

        // skip if stemmed is stop_word to
        //probably it's better to separate stop_lists, but there weren't enough time during hackaton.
        if (stop_words.find(stemmed) != stop_words.end()) continue;

        auto word_ptr = word_dict.find(stemmed);
        if (word_ptr == word_dict.end()) continue;

        WordId word_id = word_ptr->second;
        auto count_ptr = freq.find(word_id);
        if (count_ptr == freq.end())
            freq[word_id] = 1;
        else
            freq[word_id]++;
    }

    out.reserve(freq.size() + out.size());
    std::copy(freq.begin(), freq.end(), std::back_inserter(out));
    auto comparator = [](const std::pair<WordId, size_t>& a,
                         const std::pair<WordId, size_t>& b) -> bool
    {
        return a.first < b.first;
    };
    std::sort(out.begin(), out.end(), comparator);
}


std::string LDAModel::stem(const std::string& word)
{
    auto ptr = reinterpret_cast<const unsigned char *>(word.c_str());
    auto result_ptr = sb_stemmer_stem(stemmer.get(), ptr, word.length());
    return std::string(reinterpret_cast<const char*>(result_ptr));
}


void LDAModel::topics_for_bow(const BoW &bow, Predictions &out)
{
    if (bow.empty()) return;

    Eigen::VectorXf gamma(topics);
    // eigen X data structures lacks begin/end iterators, so code may looks ugly,
    for (size_t i = 0; i < topics; ++i)
    {
        gamma(i) = gamma_dist(*gen);
    }

    Eigen::VectorXf e_log_theta(topics);
    dirichlet_expectation(gamma, e_log_theta);

    for (int i = 0; i < e_log_theta.size(); ++i)
    {
        e_log_theta(i) = exp(e_log_theta(i));
    }

    Eigen::VectorXf cts(bow.size());
    Eigen::MatrixXf e_log_beta(topics, bow.size());
    for (size_t col = 0; col < bow.size(); ++col)
    {
        auto& word_stat = bow[col];

        cts(col) = word_stat.second;

        for (size_t row = 0; row < topics; ++row)
        {
            e_log_beta(row, col) = m(row, word_stat.first);
        }
    }

    Eigen::VectorXf phi_norm {e_log_theta.transpose() * e_log_beta};
    for (int i = 0; i < phi_norm.size(); ++i)
    {
        phi_norm(i) += epsilon;
    }

    for (size_t i = 0; i < iteration_to_converge; ++i)
    {
        Eigen::VectorXf last_gamma{gamma};

        Eigen::VectorXf cts_norm{cts};
        for (int j = 0; j < cts_norm.size(); ++j)
        {
            cts_norm(j) = cts(j) / phi_norm(j);
        }

        Eigen::VectorXf e_log_beta_norm{e_log_beta * cts_norm};

        Eigen::VectorXf e_log_theta_normed{e_log_theta};
        for (int j = 0; j < e_log_theta_normed.size(); ++j)
        {
            gamma(j) = e_log_theta_normed(j) * e_log_beta_norm(j) + alpha(j);
        }

        dirichlet_expectation(gamma, e_log_theta);

        for (int j = 0; j < e_log_theta.size(); ++j)
        {
            e_log_theta(j) = exp(e_log_theta(j));
        }

        Eigen::VectorXf temp_phi_norm {e_log_theta.transpose() *  e_log_beta};
        for (int j = 0; j < phi_norm.size(); ++j)
        {
            phi_norm(j) = temp_phi_norm(j) + epsilon;
        }

        float mean_change = 0;
        for (int j = 0; j < gamma.size(); ++j)
        {
            mean_change += abs(gamma(j) - last_gamma(j));
        }
        mean_change /= gamma.size();

        if (mean_change < gamma_threshold)
        {
            break;
        }
    }

    float gamma_sum = 0;
    for (int i = 0; i < gamma.size(); ++i)
    {
        gamma_sum += gamma(i);
    }

    out.reserve(out.size() + gamma.size());
    for (int i = 0; i < gamma.size(); ++i)
    {
        float p = gamma(i) / gamma_sum;
        out.push_back({i, p});
    }
}


void dirichlet_expectation(const Eigen::VectorXf& alpha,
                           Eigen::VectorXf& result)
{
    float sum_alpha = 0;
    for (int j = 0; j < alpha.size(); ++j)
    {
        sum_alpha += alpha(j);
    }

    float psi_alpha = digamma(sum_alpha);

    for (int j = 0; j < alpha.size(); ++j)
    {
        result(j) = digamma(alpha[j]) - psi_alpha;
    }
}
