#ifndef TOPIC_MODELING_H
#define TOPIC_MODELING_H

#include <memory>
#include <vector>
#include <cmath>
#include <unordered_map>
#include <unordered_set>
#include <codecvt>
#include <locale>
#include <random>

#include <eigen3/Eigen/Core>
#include "stemmer/include/libstemmer.h"


/**
 * Initiates topic modeling, must be called before every other function call from this header.
 */
void init_topic_modeling();

/**
 * Initiates russian model.
 */
void load_model_ru();

/**
 * Initiates english model.
 */
void load_model_en();


/**
 * Predict probability that <text> contains particular topic, for each topic from <category_probability>.
 * \param[in] text: string for topic classification.
 * \param[in] lang: language, used to pick a model for classification.
 * \param[out] category_probability: array of fixed size, each entry is probability of containing particular topic.
 * \return true if prediction was successfull, else false.
 */
bool predict(const std::string &text, const std::string &lang, double *category_probability) noexcept;


/**
 * Calculates digamma value for given x.
 *      Ported from python/cython gensim library.
 * \param[in] x: intial value .
 * \return calculated diamma.
 */
template<typename T>
T digamma(T x)
{
    T c = 8.5;
    T euler_mascheroni = 0.57721566490153286060;
    T r;
    T value;
    T x2;

    if ( x <= 0.000001 )
    {
        value = - euler_mascheroni - 1.0 / x + 1.6449340668482264365 * x;
        return value;
    }

    // Reduce to DIGAMA(X + N).
    value = 0.0;
    x2 = x;
    while ( x2 < c )
    {
        value = value - 1.0 / x2;
        x2 = x2 + 1.0;
    }

    // Use Stirling's (actually de Moivre's) expansion.
    r = 1.0 / x2;
    value = value + log ( x2 ) - 0.5 * r;

    r *= r;

    value = value \
            - r * ( 1.0 / 12.0  \
                    - r * ( 1.0 / 120.0 \
                            - r * ( 1.0 / 252.0 \
                                    - r * ( 1.0 / 240.0 \
                                            - r * ( 1.0 / 132.0 ) ) ) ) );

    return value;
}


/**
 * Applies digamma transformation for each value, and subtracts transformed digamma expectation from each value.
 *      Ported from python/cython gensim library.
 * \param[in] alpha: intial array.
 * \param[out] result: transformed results.
 */
void dirichlet_expectation(const Eigen::VectorXf& alpha,
                           Eigen::VectorXf& result);


/**
 * Model that performs topic classification using predict method
 */
class LDAModel
{
public:
    using WordId = size_t;
    using WordDict = std::unordered_map<std::string, WordId>;
    using StopWords = std::unordered_set<std::string>;
    using BoW = std::vector<std::pair<WordId, size_t>>;
    using TopicId = size_t;
    using Predictions = std::vector<std::pair<TopicId, float>>;
    using StrConverter = std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>>;
    using Stemmer = std::unique_ptr<struct sb_stemmer, decltype (&sb_stemmer_delete)>;
    using Mapping = std::unordered_map<size_t, std::vector<size_t>>;

private:
    size_t topics = 0;
    size_t words = 0;
    size_t iteration_to_converge = 0;
    float gamma_threshold = 0;

    Eigen::MatrixXf m;
    Eigen::VectorXf alpha;

    // used instead std::numeric_limits for test compatability with python code.
    const float epsilon = 1.1920929e-07;

    WordDict word_dict;
    StopWords stop_words;
    StrConverter converter;
    Stemmer stemmer;
    Mapping mapping;

    std::gamma_distribution<float> gamma_dist;
    std::random_device rd;
    std::unique_ptr<std::mt19937> gen;


public:

    /**
     * \param[in] lant: language of model.
     * \param[in] model_path: string, path to model's weights matrix.
     * \param[in] dict_path: string, path to dictionary of all known words.
     * \param[in] stop_words: string, path to stop words, that must be skipped.
     * \param[in] mapping: string, path to mapping from topic cluster, to particular topic probability score.
     */
    LDAModel(const std::string& lang,
             const std::string& model_path,
             const std::string& dict_path,
             const std::string& stop_words,
             const std::string& mapping);

    virtual ~LDAModel() {};

    /**
     * Predicts probability for each topic, that it is present in <text> using language <lang>
     *  \param[in] text: text for prediction.
     *  \param[in] lang: language for prediction.
     *  \param[out] category_probability: final array of probabilities for each topic.
    */
    static bool predict(const std::string& text,
                        const std::string& lang,
                        double* category_probability) noexcept;

    /**
     *  Represents text as a bag of words, some preprocessing is applied to each word,
     *      some words are dropped, rest are replaced with their ID from dictionary,
     *      each ID also contains number of occurences in text.
     *  \param[in] text: text for convertation.
     *  \param[out] out: resuling array of <token ID, number of occurences>.
    */
    void to_bow(const std::string& text, BoW& out) noexcept;

    /**
     *  Predicts prabability for each topic, that it is present in the bow.
     *  \param[in] bow: bag of words (array of <token ID, number of occurences>).
     *  \param[out] out: array of probabilities for each topic.
    */
    void topics_for_bow(const BoW& bow, Predictions& out) ;

private:
    LDAModel() = delete;

    void load_model(const std::string& model_path);

    void load_dict(const std::string& dict_path);

    void load_stop_words(const std::string& stop_words_path);

    void load_mapping(const std::string& mapping_path);

    std::string stem(const std::string& word);

};

#endif // TOPIC_MODELING_H
