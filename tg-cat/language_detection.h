#ifndef LANGUAGE_DETECTION_H
#define LANGUAGE_DETECTION_H

#include <string>
#include <vector>
#include <utility>


/**
 * Initiates language detection model, must be called before every other function call from this header.
 */
int init_language_detection_model();


/**
 * Calculates digamma value for given x. Ported from python/cython gensim library.
 * \param[in] intial value x.
 * \return calculated diamma.
 */
void detect_language(const std::string& s,
                     std::vector<std::pair<float, std::string>> &results,
                     float p_threthold = 0.05);

#endif // LANGUAGE_DETECTION_H
