#### configure

mkdir build && cd build

cmake -DCMAKE\_BUILD\_TYPE=Release ..

#### build 

cmake --build .

#### evaluate

cd tg-cat-tester/

./tgcat-tester language ../../contest-static/dc0202-input.txt dc0202-language.txt

less dc0202-language.txt


./tgcat-tester category ../../contest-static/dc0202-input.txt dc0202-categories.txt

less dc0202-language.txt
